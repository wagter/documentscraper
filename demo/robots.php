<?php

use Wagter\DocumentScraper\Loader\CurlUrlLoader;
use Wagter\DocumentScraper\Loader\InvalidUrlException;
use Wagter\DocumentScraper\Loader\UrlLoader;
use Wagter\DocumentScraper\Loader\UrlLoaderException;
use Wagter\DocumentScraper\Tag\TagList;
use Wagter\DocumentScraper\TagScraper;
use Wagter\DocumentScraper\Tag\RobotsMetaTag;
use Wagter\DocumentScraper\TagScraperException;

require_once '../vendor/autoload.php';


$loader  = new CurlUrlLoader();
$tags    = new TagList( [ new RobotsMetaTag() ] );
$scraper = new TagScraper( $tags );

echo "**beep boop** Hello human, I am Scraperbot. I was created to scrape robots meta-tags from URL at your service! **beep boop**\n";
echo "**beep boop** Please input URL and press RETURN: ";

$url = rtrim( fgets( STDIN ) );

echo "**beep boop** Scraperbot is processing your input. Please wait... **beep boop**\n";

try {
    
    $doc          = $loader->load( $url );
    $httpCode     = $loader->getLatestHttpCode();
    $responseTime = $loader->getLatestResponseTime();
    
    echo "**beep boop** Scraperbot received HTTP response with status code \"{$httpCode}\" in {$responseTime} seconds... **beep boop**\n";
    
    $result = $scraper->scrape( $doc );
    
    echo "**beep boop** Scraperbot found robots meta-tag with \"{$result}\"... **beep boop**\n";
    
} catch ( InvalidUrlException $e ) {
    echo "**beep boop** ERRORROR! Scraperbot can not process input. Please input valid URL... **beep boop**\n";
} catch ( UrlLoaderException $e ) {
    echo "**beep boop** ERRORROR! Scraperbot got no valid response from URL... **beep boop**\n";
} catch ( TagScraperException $e ) {
    echo "**beep boop** ERRORROR! Scraperbot could not find robots meta-tag in the result from input... **beep boop**\n";
} catch ( Exception $e ) {
    echo sprintf( '**beep boop** ERRORROR! %s **beep boop** %s', $e->getMessage(), "\n" );
}

echo "**beep boop** Scraperbot is terminating...\n";
die();