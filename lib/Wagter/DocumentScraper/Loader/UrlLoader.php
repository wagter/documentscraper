<?php

namespace Wagter\DocumentScraper\Loader;

/**
 * Use to load a (HTML) document from a URL
 *
 * Class DocumentLoader
 * @package Jrswgtr\UrlPreview\Loader\Document
 *
 * @author Joris Wagter <http://wagter.net>
 */
class UrlLoader implements UrlLoaderInterface
{
    /**
     * {@inheritdoc}
     */
    function load( string $url ): string
    {
        $this->validateUrl( $url );
        
        $file = @fopen( $url, 'r' );
        
        if ( $file === false ) {
            throw new UrlLoaderException( sprintf( 'The URL: "%s", did not return a result.', $url ) );
        }
        
        return $this->getFileContent( $file );
    }
    
    /**
     * @param string $url
     *
     * @throws InvalidUrlException
     */
    protected function validateUrl( string $url )
    {
        if ( filter_var( $url, FILTER_VALIDATE_URL ) === false ) {
            throw new InvalidUrlException(
                sprintf( 'The argument of UrlLoader::load( string $url ) (%s) is not a valid URL.', $url )
            );
        }
    }
    
    /**
     * Get the content of a file
     *
     * @param resource $file the file handler
     *
     * @return string the file's content
     */
    protected function getFileContent( $file ): string
    {
        $content = '';
        
        while ( ! feof( $file ) ) {
            $content .= fgets( $file, 1024 );
        }
        
        return $this->sanitizeContent( $content );
    }
    
    /**
     * Sanitize content
     *
     * @param string $content the content to sanitize
     *
     * @return string the sanitized content
     */
    protected function sanitizeContent( string $content ): string
    {
        $content = str_replace( [ "\r", "\n" ], ' ', $content );
        $content = str_replace( '  ', ' ', $content );
        $content = str_replace( '\'', '"', $content );
        
        return $content;
    }
}