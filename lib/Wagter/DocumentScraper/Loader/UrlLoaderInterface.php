<?php

namespace Wagter\DocumentScraper\Loader;

/**
 * To implement a document loader to load a (HTML) document from a URL.
 *
 * Interface DocumentLoaderInterface
 * @package Jrswgtr\UrlPreview\Loader
 *
 * @author Joris Wagter <http://wagter.net>
 */
interface UrlLoaderInterface
{
	/**
	 * Try to load a document from a URL.
	 *
	 * @param string $url the URL to load the document from
	 *
	 * @return string the URL's returned content
     * @throws InvalidUrlException if the given argument is not a valid URL
     * @throws UrlLoaderException if the loader failed to retrieve a result from the URL
	 */
	function load( string $url ): string;
}