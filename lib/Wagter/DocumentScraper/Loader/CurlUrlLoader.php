<?php


namespace Wagter\DocumentScraper\Loader;


class CurlUrlLoader extends UrlLoader
{
    /**
     * @var int
     */
    private $latestHttpCode = 0;
    
    /**
     * @var float
     */
    private $latestResponseTime = 0.0;
    
    public function load( string $url ): string
    {
        $this->validateUrl( $url );
        
        $user_agent = 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)';
        
        $options = [
            
            CURLOPT_CUSTOMREQUEST  => "GET",        //set request type post or get
            CURLOPT_POST           => false,        //set to GET
            CURLOPT_USERAGENT      => $user_agent, //set user agent
//            CURLOPT_COOKIEFILE     => "cookie.txt", //set cookie file
//            CURLOPT_COOKIEJAR      => "cookie.txt", //set cookie jar
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => false,    // don't return headers
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
        ];
        
        $ch = curl_init( $url );
        curl_setopt_array( $ch, $options );
        $content = curl_exec( $ch );
        $err     = curl_errno( $ch );
        $errmsg  = curl_error( $ch );
        $info    = curl_getinfo( $ch );
        
        $this->latestHttpCode     = $info['http_code'];
        $this->latestResponseTime = $info['total_time'];
        
        if ( strlen( $errmsg ) > 0 ) {
            throw new UrlLoaderException( $errmsg );
        }

        curl_close( $ch );
        
        return $this->sanitizeContent( $content );
    }
    
    public function getLatestHttpCode(): int
    {
        return $this->latestHttpCode;
    }
    
    public function getLatestResponseTime(): float
    {
        return $this->latestResponseTime;
    }
}