<?php

namespace Wagter\DocumentScraper\Loader;

/**
 * Class DummyDocumentLoader
 * @package Jrswgtr\UrlPreview\Loader\Document
 *
 * @author Joris Wagter <http://wagter.net>
 */
class DummyUrlLoader implements UrlLoaderInterface
{
	/**
	 * {@inheritdoc}
	 */
	function load( string $url ): string
	{
		return
			'<html lang="en_GB">' .
			'<head>' .
			
			'<meta name="description" content="Lorem ipsum dolor sit amet." />' .
            '<meta name="robots" content="noindex,nofollow,noarchive" />' .
			'<meta property="og:description" content="Lorem ipsum dolor sit amet." />' .
			'<meta property="twitter:description" content="Lorem ipsum dolor sit amet." />' .
			
			'<meta property="og:image" content="https://via.placeholder.com/300.png" />' .
			'<meta property="twitter:image" content="https://via.placeholder.com/400.png" />' .
			
			'<title>DocumentScraper</title>' .
			'<meta property="og:title" content="Jrswgtr UrlPreview" />' .
			'<meta property="twitter:title" content="Jrswgtr UrlPreview" />' .
			
			'</head>' .
			'<body>' .
			
			'<h1>About Jrswgtr UrlPreview</h1>' .
			'<img src="https://via.placeholder.com/500.png"  alt=""/>' .
			'<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate</p>' .
			
			'</body>' .
			'</html>';
	}
}