<?php

namespace Wagter\DocumentScraper\Loader;

/**
 * Use to load a (HTML) document from a URL and save it to a file before returning
 *
 * Class DocumentLoader
 * @package Jrswgtr\UrlPreview\Loader\Document
 *
 * @author Joris Wagter <http://wagter.net>
 */
class DebugUrlLoader extends UrlLoader
{
	/**
	 * @var string the directory to store the HTML files
	 */
	private $dir;
	
	/**
	 * DebugDocumentLoader constructor.
	 *
	 * @param string $dir
	 */
	public function __construct( string $dir )
	{
		if ( ! is_dir( $dir ) ) {
			mkdir( $dir, 0755, true );
		}
		
		$this->dir = $dir;
	}
	
	/**
	 * {@inheritdoc}
	 */
	function load( string $url ): string
	{
		$content = parent::load( $url );
		
		if ( $content === null ) {
			return null;
		}
		
		$path = sprintf( '%s/%s.%s.html',
			$this->dir,
			date( 'Y-m-d-H:i:s' ),
			hash( 'md5', $url )
		);
		
		file_put_contents( $path, $content );
		
		return $content;
	}
}