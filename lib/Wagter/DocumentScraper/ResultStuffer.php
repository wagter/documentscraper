<?php

namespace Wagter\DocumentScraper;

use Wagter\DocumentScraper\Map\ResultMap;

class ResultStuffer implements ResultStufferInterface
{
	/**
	 * {@inheritdoc}
	 */
	public function stuff( ResultMap $results, $object )
	{
		foreach ( $results as $key => $result ) {
			$setter = 'set' . ucfirst( $key );
			if ( method_exists( $object, $setter ) ) {
				$object->$setter( $result );
			} else if ( property_exists( $object, $key ) ) {
				$object->$key = $result;
			}
		}
	}
}