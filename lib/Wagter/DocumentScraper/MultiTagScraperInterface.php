<?php

namespace Wagter\DocumentScraper;

use Wagter\DocumentScraper\Map\ResultMap;

/**
 * Interface MultiTagScraperInterface
 *
 * Use implement a scraper using multiple TagScraperInterface instances
 *
 * @package Wagter\DocumentScraper
 */
interface MultiTagScraperInterface
{
    /**
     * @param string $document
     *
     * @return ResultMap
     */
	public function scrape( string $document ): ResultMap;
    
    /**
     * @param string $key
     * @param TagScraperInterface $tagScraper
     *
     * @return MultiTagScraper
     */
	public function setTagScraper( string $key, TagScraperInterface $tagScraper ): MultiTagScraper;
}