<?php

namespace Wagter\DocumentScraper\Cache;

use Wagter\DocumentScraper\Map\ResultMap;

/**
 * Basic filesystem cache provider for storing an loading UrlPreview models
 *
 * Please feel free to implement your own cache provider for advanced caching methods
 * @see CacheProviderInterface
 *
 * Class FileSystemCacheProvider
 * @package Jrswgtr\UrlPreview\Cache
 *
 * @author Joris Wagter <http://wagter.net>
 */
class FileSystemCacheProvider implements CacheProviderInterface
{
	/**
	 * @var string the path to store the cache files
	 */
	private $path;
	
	/**
	 * @var int the cache lifetime in seconds
	 */
	private $lifeTime;
	
	/**
	 * @var array containing pairs of URL's and cache file paths
	 */
	private $urlFilePaths = [];
	
	/**
	 * FileSystemCacheProvider constructor
	 *
	 * @param string $path the absolute path to store the cache files
	 * @param int $lifeTime
	 */
	public function __construct( string $path, int $lifeTime = 0 )
	{
		$this->path     = $path;
		$this->lifeTime = $lifeTime;
		
		if ( ! is_dir( $this->path ) ) {
			mkdir( $this->path, 0755, true );
		}
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function put( string $url, ResultMap $resultMap ): CacheProviderInterface
	{
		$hash = hash( 'md5', $url );
		
		$json = $resultMap->__toString();
		
		file_put_contents( sprintf( '%s/%s.json', $this->path, $hash ), $json );
		
		return $this;
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function get( string $url ): ResultMap
	{
		if ( ! $this->has( $url ) ) {
			return null;
		}
		
		$hash = hash( 'md5', $url );
		
		$json = file_get_contents( sprintf( '%s/%s.json', $this->path, $hash ) );
		
		$array = json_decode( $json, true );
		
		if ( $array === null ) {
			return null;
		}
		
		$resultMap = new ResultMap();
		
		foreach ( $array as $key => $result ) {
			$resultMap->put( $key, $result );
		}
		
		return $resultMap;
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function has( string $url ): bool
	{
		return file_exists( $this->getFilePath( $url ) );
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function valid( string $url ): bool
	{
		if ( $this->lifeTime === 0 ) {
			return true;
		}
		
		$fileTime = filemtime( $this->getFilePath( $url ) );
		
		if ( $fileTime === false ) {
			return false;
		}
		
		$time = time();
		
		return $time - $fileTime <= $this->lifeTime;
	}
	
	private function getFilePath( string $url ): string
	{
		if ( array_key_exists( $url, $this->urlFilePaths ) ) {
			return $this->urlFilePaths[ $url ];
		}
		
		$filePath = sprintf( '%s/%s.json', $this->path, hash( 'md5', $url ) );
		
		$this->urlFilePaths[ $url ] = $filePath;
		
		return $filePath;
	}
}