<?php

namespace Wagter\DocumentScraper\Cache;


use Wagter\DocumentScraper\Map\ResultMap;

interface CacheProviderInterface
{
	function put( string $url, ResultMap $resultMap ): CacheProviderInterface;
	
	function has( string $url ): bool;
	
	function get( string $url ): ResultMap;
	
	function valid( string $url ): bool;
}