<?php

namespace Wagter\DocumentScraper;

use Wagter\DocumentScraper\Tag\TagInterface;
use Wagter\DocumentScraper\Tag\TagList;

/**
 * To scrape documents for tags.
 *
 * Class Scraper
 * @package Wagter\DocumentScraper
 *
 * @author Joris Wagter <http://wagter.net>
 */
class TagScraper implements TagScraperInterface
{
    /**
     * @var TagList the tags to scrape the document for
     */
    private $tags;
    
    /**
     * Scraper constructor.
     *
     * @param TagList $tags the tags to scrape the document for
     */
    public function __construct( TagList $tags = null )
    {
        if ( $tags !== null ) {
            $this->tags = $tags;
            
            return;
        }
        $this->tags = new TagList();
    }
    
    /**
     * {@inheritdoc}
     */
    public function scrape( string $document ): string
    {
        foreach ( $this->tags as $tag ) {
            
            $match = $tag->match( $document );
            
            if ( $match ) {
                return $match;
            }
        }
        
        throw new TagScraperException( 'No matching tags were found.' );
    }
    
    /**
     * {@inheritdoc}
     */
    public function addTag( TagInterface $tag ): TagScraperInterface
    {
        $this->tags->add( $tag );
        
        return $this;
    }
}