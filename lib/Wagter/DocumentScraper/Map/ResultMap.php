<?php

namespace Wagter\DocumentScraper\Map;

use Wagter\DocumentScraper\TagScraperInterface;

/**
 * A map to associate keys with scrapers
 *
 * Class ScraperMap
 * @package Wagter\DocumentScraper
 */
class ResultMap extends AbstractMap
{
	/**
	 * ScraperMap constructor.
	 *
	 * @param array $map
	 */
	public function __construct( array $map = [] )
	{
		foreach ( $map as $key => $scraper ) {
			$this->put( $key, $scraper );
		}
	}
	
	/**
	 * Put a result in the map
	 *
	 * @param string $key
	 * @param string $result
	 *
	 * @return ResultMap
	 */
	public function put( string $key, string $result ): ResultMap
	{
		$this->map[ $key ] = $result;
		
		if ( ! $this->has( $key ) ) {
			$this->keys[] = $key;
		}
		
		return $this;
	}
	
	/**
	 * Get a result from the map
	 *
	 * @param string $key
	 *
	 * @return TagScraperInterface
	 */
	public function get( string $key ): string
	{
		return $this->map[ $key ];
	}
}