<?php

namespace Wagter\DocumentScraper\Map;

use Wagter\DocumentScraper\TagScraperInterface;

/**
 * A map to associate keys with scrapers
 *
 * Class ScraperMap
 * @package Wagter\DocumentScraper
 */
class TagScraperMap extends AbstractMap
{
	/**
	 * ScraperMap constructor.
	 *
	 * @param array $map
	 */
	public function __construct( array $map = [] )
	{
		foreach ( $map as $key => $scraper ) {
			$this->put( $key, $scraper );
		}
	}
	
	/**
	 * Put a scraper in the map
	 *
	 * @param string $key
	 * @param TagScraperInterface $scraper
	 *
	 * @return TagScraperMap
	 */
	public function put( string $key, TagScraperInterface $scraper ): TagScraperMap
	{
		$this->map[ $key ] = $scraper;
		
		if ( ! $this->has( $key ) ) {
			$this->keys[] = $key;
		}
		
		return $this;
	}
	
	/**
	 * Get a scraper from the map
	 *
	 * @param string $key
	 *
	 * @return TagScraperInterface
	 */
	public function get( string $key ): TagScraperInterface
	{
		return $this->map[ $key ];
	}
}