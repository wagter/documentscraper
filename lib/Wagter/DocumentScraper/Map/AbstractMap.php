<?php

namespace Wagter\DocumentScraper\Map;

/**
 * Class AbstractMap
 * @package Wagter\DocumentScraper\Map
 */
abstract class AbstractMap implements \Iterator, \Countable
{
	/**
	 * The map containing key, ScraperInterface pairs
	 *
	 * @var array
	 */
	protected $map = [];
	
	/**
	 * The map containing the keys for the iterator
	 *
	 * @var array
	 */
	protected $keys = [];
	
	/**
	 * The current iterator index
	 *
	 * @var int
	 */
	protected $current = 0;
	
	/**
	 * Check if the map contains a certain key
	 *
	 * @param string $key
	 *
	 * @return bool
	 */
	public function has( string $key = null ): bool
	{
		return $key !== null
			? in_array( $key, $this->keys )
			: false;
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function current()
	{
		return $this->map[ $this->keys[ $this->current ] ];
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function next()
	{
		$this->current ++;
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function key()
	{
		if ( isset( $this->keys[ $this->current ] ) ) {
			return $this->keys[ $this->current ];
		}
		
		return null;
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function valid()
	{
		return isset($this->keys[ $this->current ]);
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function rewind()
	{
		$this->current = 0;
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function count()
	{
		return count( $this->keys );
	}
}