<?php

namespace Wagter\DocumentScraper;

use Wagter\DocumentScraper\Loader\UrlLoaderException;
use Wagter\DocumentScraper\Map\ResultMap;

interface UrlScraperInterface
{
	/**
	 * @param string $url
	 *
	 * @return ResultMap
     * @throws UrlLoaderException
	 */
	public function scrape( string $url ): ResultMap;
}