<?php

namespace Wagter\DocumentScraper;


use Wagter\DocumentScraper\Tag\TagInterface;

/**
 * Interface ScraperInterface
 *
 * To implement a Scraper for scraping documents.
 *
 * @package Wagter\DocumentScraper
 *
 * @author Joris Wagter <http://wagter.net>
 */
interface TagScraperInterface
{
	/**
	 * Scrape a document for tags.
	 *
	 * @param string $document the document to scrape for tags
	 *
	 * @return string the value of the matched tag
     *
     * @throws TagScraperException when a tag failed to match
	 */
	public function scrape( string $document ): ?string;
	
	/**
	 * Add a tag to the scraper.
	 *
	 * @param TagInterface $tag a tag to scrape the document for
	 *
	 * @return TagScraperInterface
	 */
	public function addTag( TagInterface $tag ): TagScraperInterface;
}