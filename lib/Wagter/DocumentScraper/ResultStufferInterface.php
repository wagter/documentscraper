<?php

namespace Wagter\DocumentScraper;

use Wagter\DocumentScraper\Map\ResultMap;

interface ResultStufferInterface
{
	/**
	 * Try to stuff results in an object
	 *
	 * @param ResultMap $results
	 * @param mixed $object
	 */
	public function stuff( ResultMap $results, $object );
}