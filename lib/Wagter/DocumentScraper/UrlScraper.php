<?php

namespace Wagter\DocumentScraper;

use Wagter\DocumentScraper\Cache\CacheProviderInterface;
use Wagter\DocumentScraper\Loader\UrlLoaderInterface;
use Wagter\DocumentScraper\Map\ResultMap;

class UrlScraper implements UrlScraperInterface
{
    /**
     * @var UrlLoaderInterface
     */
    private $documentLoader;
    
    /**
     * @var MultiTagScraper
     */
    private $documentScraper;
    
    /**
     * @var CacheProviderInterface
     */
    private $cacheProvider;
    
    /**
     * UrlScraper constructor.
     *
     * @param UrlLoaderInterface $documentLoader
     * @param MultiTagScraperInterface $documentScraper
     * @param CacheProviderInterface $cacheProvider
     */
    public function __construct(
        UrlLoaderInterface $documentLoader,
        MultiTagScraperInterface $documentScraper,
        CacheProviderInterface $cacheProvider = null
    ) {
        $this->documentLoader  = $documentLoader;
        $this->documentScraper = $documentScraper;
        $this->cacheProvider   = $cacheProvider;
    }
    
    /**
     * {@inheritdoc}
     */
    public function scrape( string $url ): ResultMap
    {
        $url = $this->sanitizeUrl( $url );
        
        if ( $this->cacheProvider !== null && $this->cacheProvider->has( $url ) ) {
            return $this->cacheProvider->get( $url );
        }
        
        $document  = $this->documentLoader->load( $url );
        $resultMap = $this->documentScraper->scrape( $document );
        $resultMap->put( 'url', $url );
        
        if ( $this->cacheProvider !== null ) {
            $this->cacheProvider->put( $url, $resultMap );
        }
        
        return $resultMap;
    }
    
    /**
     * Sanitize a URL
     *
     * @param string $url the URL to sanitize
     *
     * @return string the sanitized URL
     */
    private function sanitizeUrl( string $url ): string
    {
        return htmlspecialchars( trim( $url ), ENT_QUOTES, 'ISO-8859-1', true );
    }
}