<?php

namespace Wagter\DocumentScraper;

use Wagter\DocumentScraper\Map\ResultMap;
use Wagter\DocumentScraper\Map\TagScraperMap;

/**
 * Class MultiTagScraper
 *
 * Use to scrape a HTML document using multiple TagScraperInterface instances
 *
 * @package Wagter\DocumentScraper
 */
class MultiTagScraper implements MultiTagScraperInterface
{
	/**
	 * @var TagScraperMap
	 */
	private $tagScrapers;
	
	/**
	 * DocumentScraper constructor.
	 *
	 * @param TagScraperMap $tagScrapers
	 */
	public function __construct( TagScraperMap $tagScrapers = null )
	{
		$this->tagScrapers = $tagScrapers !== null
			? $tagScrapers
			: new TagScraperMap();
	}
    
    /**
     * {@inheritDoc}
     */
	public function scrape( string $document ): ResultMap
	{
		$resultsMap = new ResultMap();
		
		foreach ( $this->tagScrapers as $key => $scraper ) {
			$result = $scraper->scrape( $document );
			if ( $result !== null ) {
				$resultsMap->put( $key, $result );
			}
		}
		
		return $resultsMap;
	}
    
    /**
     * {@inheritDoc}
     */
	public function setTagScraper( string $key, TagScraperInterface $tagScraper ): MultiTagScraper
	{
		$this->tagScrapers->put( $key, $tagScraper );
		
		return $this;
	}
}