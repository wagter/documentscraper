<?php

namespace Wagter\DocumentScraper\Tag;

/**
 * Match a robots meta tag in a HTML document
 *
 * <meta name="robots" content="Site description" />
 *
 * Class RobotsMetaTag
 * @package Wagter\DocumentScraper\Tag
 *
 * @author Joris Wagter <http://wagter.net>
 */
class RobotsMetaTag extends AbstractMetaTag
{
	/**
	 * {@inheritdoc}
	 */
	function match( string $document ): ?string
	{
		return $this->matchByName( 'robots', $document );
	}
}