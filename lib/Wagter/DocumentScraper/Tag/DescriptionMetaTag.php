<?php

namespace Wagter\DocumentScraper\Tag;

/**
 * Match a description meta tag in a HTML document
 *
 * <meta name="description" content="Site description" />
 *
 * Class DescriptionMetaTag
 * @package Wagter\DocumentScraper\Tag
 *
 * @author Joris Wagter <http://wagter.net>
 */
class DescriptionMetaTag extends AbstractMetaTag
{
	/**
	 * {@inheritdoc}
	 */
	function match( string $document ): ?string
	{
		return $this->matchByName( 'description', $document );
	}
}