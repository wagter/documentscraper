<?php

namespace Wagter\DocumentScraper\Tag;

/**
 * Match the first image tag to come across that matches the minimum and maximum dimensions.
 *
 * <img src="http://url-to-image.com/image.jpg" />
 *
 * Class ImageTag
 * @package Wagter\DocumentScraper\Tag
 *
 * @author Joris Wagter <http://wagter.net>
 */
class ImageTag implements TagInterface
{
	/**
	 * @var int the minimum width of the matched image tag
	 */
	private $minWidth;
	
	/**
	 * @var int the maximum width of the matched image tag
	 */
	private $maxWidth;
	
	/**
	 * @var int the minimum height of the matched image tag
	 */
	private $minHeight;
	
	/**
	 * @var int the maximum height of the matched image tag
	 */
	private $maxHeight;
	
	/**
	 * ImageTag constructor.
	 *
	 * @param int $minWidth
	 * @param int $maxWidth
	 * @param int $minHeight
	 * @param int $maxHeight
	 */
	public function __construct(
		int $minWidth = 300,
		int $maxWidth = 1920,
		int $minHeight = 200,
		int $maxHeight = 1080
	) {
		$this->minWidth  = $minWidth;
		$this->maxWidth  = $maxWidth;
		$this->minHeight = $minHeight;
		$this->maxHeight = $maxHeight;
	}
	
	/**
	 * {@inheritdoc}
	 */
	function match( string $document ): ?string
	{
		preg_match_all( '/<img[^>]*src=[\"|\'](.*)[\"|\']/Ui', $document, $matches, PREG_PATTERN_ORDER );
		
		if ( count( $matches ) < 2 ) {
			return null;
		}
		
		foreach ( $matches[1] as $match ) {
			try {
				list( $width, $height ) = getimagesize( $match );
				if ( $width >= $this->minWidth &&
				     $width <= $this->maxWidth &&
				     $height >= $this->minHeight &&
				     $height <= $this->maxHeight
				) {
					return $match;
				}
			} catch ( \Exception $e ) {
			}
		}
		
		return null;
	}
}