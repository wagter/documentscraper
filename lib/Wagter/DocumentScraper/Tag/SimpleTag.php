<?php

namespace Wagter\DocumentScraper\Tag;

/**
 * Match a tag in a HTML document
 *
 * <tagname>Tag content</tagname>
 *
 * Class TitleTag
 * @package Wagter\DocumentScraper\Tag
 *
 * @author Joris Wagter <http://wagter.net>
 */
class SimpleTag implements TagInterface
{
	/**
	 * @var string the name of the tag
	 */
	private $tagName;
	
	/**
	 * SimpleTag constructor.
	 *
	 * @param string $tagName
	 */
	public function __construct( string $tagName )
	{
		$this->tagName = $tagName;
	}
	
	/**
	 * {@inheritdoc}
	 */
    public function match( string $document ): ?string
	{
		preg_match_all(
			'/<' . $this->tagName . '>(.+)<\/' . $this->tagName . '>/i',
			$document,
			$match,
			PREG_PATTERN_ORDER
		);
		
		if ( is_string( $match[1] ) ) {
			return $match[1];
		} else if ( is_array( $match[1] ) && count( $match[1] ) > 0 ) {
			return $match[1][0];
		}
		
		return null;
	}
}