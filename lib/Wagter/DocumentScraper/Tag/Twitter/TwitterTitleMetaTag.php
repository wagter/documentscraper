<?php

namespace Wagter\DocumentScraper\Tag\Twitter;

use Wagter\DocumentScraper\Tag\AbstractMetaTag;

/**
 * Match a twitter:image meta tag in a HTML document
 *
 * <meta property="twitter:title" content="Site Title" />
 *
 * Class TwitterTitleMetaTag
 * @package Wagter\DocumentScraper\Tag
 *
 * @author Joris Wagter <http://wagter.net>
 */
class TwitterTitleMetaTag extends AbstractMetaTag
{
	/**
	 * {@inheritdoc}
	 */
	function match( string $document ): ?string
	{
		return $this->matchByProperty( 'twitter:title', $document );
	}
}