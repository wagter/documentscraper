<?php

namespace Wagter\DocumentScraper\Tag\Twitter;

use Wagter\DocumentScraper\Tag\AbstractMetaTag;

/**
 * Match a twitter:image meta tag in a HTML document
 *
 * <meta property="twitter:image" content="http://url-to-image.com/image.jpg" />
 *
 * Class TwitterImageMetaTag
 * @package Wagter\DocumentScraper\Tag
 *
 * @author Joris Wagter <http://wagter.net>
 */
class TwitterImageMetaTag extends AbstractMetaTag
{
	/**
	 * {@inheritdoc}
	 */
	function match( string $document ): ?string
	{
		return $this->matchByProperty( 'twitter:image', $document );
	}
}