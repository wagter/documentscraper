<?php

namespace Wagter\DocumentScraper\Tag;

/**
 * Class ParagraphTag
 * @package Wagter\DocumentScraper\Tag
 */
class ParagraphTag implements TagInterface
{
	private $minLength;
	
	private $maxLength;
	
	public function __construct( int $minLength = 50, int $maxLength = 350 )
	{
		$this->minLength = $minLength;
		$this->maxLength = $maxLength;
	}
	
	/**
	 * Match the tag in a document.
	 *
	 * @param string $document the document to match the tag in.
	 *
	 * @return null|string the content of the the matched tag or null if no match.
	 */
	public function match( string $document ): ?string
	{
		preg_match_all(
			'/<p>(.+)<\/p>/i',
			$document,
			$match,
			PREG_PATTERN_ORDER
		);
		
		if ( is_string( $match[1] ) && $this->hasCorrectLength( $match[1] ) ) {
			return $match[1];
		} else if ( is_array( $match[1] ) && count( $match[1] ) > 0 ) {
			foreach ( $match[1] as $m ) {
				if ( $this->hasCorrectLength( $m ) ) {
					return $m;
				}
			}
		}
		
		return null;
	}
	
	/**
	 * Check if the string has the correct length.
	 *
	 * @param string $string
	 *
	 * @return bool
	 */
	private function hasCorrectLength( string $string ): bool
	{
		$length = strlen( $string );
		
		return
			$length >= $this->minLength &&
			$length <= $this->maxLength;
	}
}