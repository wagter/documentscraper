<?php


namespace Wagter\DocumentScraper\Tag;

use Countable;
use Iterator;

class TagList implements Iterator, Countable
{
    /**
     * @var TagInterface[]
     */
    private $list = [];
    
    /**
     * @var int
     */
    private $currentIndex = 0;
    
    /**
     * TagList constructor.
     *
     * @param TagInterface[] $list
     */
    public function __construct( array $list = [] )
    {
        foreach ( $list as $item ) {
            $this->add( $item );
        }
    }
    
    /**
     * Add a TagInterface instance to the list
     *
     * @param TagInterface $tag
     *
     * @return $this
     */
    public function add( TagInterface $tag ): self
    {
        $this->list[] = $tag;
        
        return $this;
    }
    
    /**
     * Remove a TagInterface instance from the list by index
     *
     * @param int $index
     *
     * @return $this
     */
    public function remove( int $index ): self
    {
        if ( isset( $this->list[ $index ] ) ) {
            unset( $this->list[ $index ] );
        }
        
        return $this;
    }
    
    /**
     * Get a TagInterface instance from the list by index, returns NULL if index is not valid
     *
     * @param int $index
     *
     * @return TagInterface|null
     */
    public function get( int $index ): ?TagInterface
    {
        if ( isset( $this->list[ $index ] ) ) {
            return $this->list[ $index ];
        }
        
        return null;
    }
    
    /**
     * @inheritDoc
     */
    public function current()
    {
        return $this->list[ $this->currentIndex ];
    }
    
    /**
     * @inheritDoc
     */
    public function next()
    {
        $this->currentIndex ++;
    }
    
    /**
     * @inheritDoc
     */
    public function key()
    {
        return $this->currentIndex;
    }
    
    /**
     * @inheritDoc
     */
    public function valid()
    {
        return isset( $this->list[ $this->currentIndex ] );
    }
    
    /**
     * @inheritDoc
     */
    public function rewind()
    {
        $this->currentIndex = 0;
    }
    
    /**
     * @inheritDoc
     */
    public function count()
    {
        return count( $this->list );
    }
}