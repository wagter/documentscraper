<?php

namespace Wagter\DocumentScraper\Tag;

/**
 * Match a meta tag by name or property
 *
 * Class AbstractMetaTag
 * @package Wagter\DocumentScraper\Tag
 *
 * @author Joris Wagter <http://wagter.net>
 */
abstract class AbstractMetaTag implements TagInterface
{
	/**
	 * Match a meta tag by name
	 *
	 * <meta name="some_name" content="Some content" />
	 *
	 * @param string $name the name value of the meta tag to be matched
	 * @param string $document the document to match the meta tag in
	 *
	 * @return null|string the content value of the meta tag or null if no match
	 */
	protected function matchByName( string $name, string $document ): ?string
	{
		preg_match( '/<meta name="' . $name . '" content="(.*?)"/', $document, $match );
		
		if ( count( $match ) < 2 ) {
			return '';
		}
		
		if ( count( $match ) > 1 && is_string( $match[1] ) ) {
			return $match[1];
		} else if ( is_array( $match[1] ) && count( $match[1] ) > 0 ) {
			return $match[1][0];
		}
		
		return null;
	}
	
	/**
	 * Match a meta tag by property
	 *
	 * <meta property="some_property" content="Some content" />
	 *
	 * @param string $property the property value of the meta tag to be matched
	 * @param string $document the document to match the meta tag in
	 *
	 * @return null|string the content value of the meta tag or null if no match
	 */
	protected function matchByProperty( string $property, string $document ): ?string
	{
		preg_match( '/<meta property="' . $property . '" content="(.*?)"/', $document, $match );
		
		if ( count( $match ) < 2 ) {
			return '';
		}
		
		if ( count( $match ) > 1 && is_string( $match[1] ) ) {
			return $match[1];
		} else if ( is_array( $match[1] ) && count( $match[1] ) > 0 ) {
			return $match[1][0];
		}
		
		return null;
	}
}