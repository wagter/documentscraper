<?php

namespace Wagter\DocumentScraper\Tag;

/**
 * Match a title meta tag in a HTML document
 *
 * <title>Site Title</title>
 *
 * Class TitleTag
 * @package Wagter\DocumentScraper\Tag
 *
 * @author Joris Wagter <http://wagter.net>
 */
class TitleTag extends SimpleTag
{
	/**
	 * TitleTag constructor.
	 */
	public function __construct()
	{
		parent::__construct( 'title' );
	}
}