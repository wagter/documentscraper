<?php

namespace Wagter\DocumentScraper\Tag;

/**
 * To implement a tag to match in a document.
 *
 * Interface TagInterface
 * @package Wagter\DocumentScraper\Tag
 *
 * @author Joris Wagter <http://wagter.net>
 */
interface TagInterface
{
	/**
	 * Match the tag in a document.
	 *
	 * @param string $document the document to match the tag in.
	 *
	 * @return null|string the content of the the matched tag or null if no match.
	 */
	public function match( string $document ): ?string;
}