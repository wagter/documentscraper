<?php

namespace Wagter\DocumentScraper\Tag\OpenGraph;

use Wagter\DocumentScraper\Tag\AbstractMetaTag;

/**
 * Match a og:site_name meta tag in a HTML document
 *
 * <meta property="og:url" content="Site Name" />
 *
 * Class OgSiteNameMetaTag
 * @package Wagter\DocumentScraper\Tag
 *
 * @author Joris Wagter <http://wagter.net>
 */
class OgSiteNameMetaTag extends AbstractMetaTag
{
	/**
	 * {@inheritdoc}
	 */
	function match( string $document ): ?string
	{
		return $this->matchByProperty( 'og:site_name', $document );
	}
}