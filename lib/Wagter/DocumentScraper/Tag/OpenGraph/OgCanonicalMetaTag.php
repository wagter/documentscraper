<?php

namespace Wagter\DocumentScraper\Tag\OpenGraph;

use Wagter\DocumentScraper\Tag\AbstractMetaTag;

/**
 * Match a og:url meta tag in a HTML document
 *
 * <meta property="og:url" content="http://canonical-url.com" />
 *
 * Class OgCanonicalMetaTag
 * @package Wagter\DocumentScraper\Tag
 *
 * @author Joris Wagter <http://wagter.net>
 */
class OgCanonicalMetaTag extends AbstractMetaTag
{
	/**
	 * {@inheritdoc}
	 */
	function match( string $document ): ?string
	{
		return $this->matchByProperty( 'og:url', $document );
	}
}