<?php

namespace Wagter\DocumentScraper\Tag\OpenGraph;

use Wagter\DocumentScraper\Tag\AbstractMetaTag;

/**
 * Match a og:description meta tag in a HTML document
 *
 * <meta property="og:description" content="Site description" />
 *
 * Class OgDescriptionMetaTag
 * @package Wagter\DocumentScraper\Tag
 *
 * @author Joris Wagter <http://wagter.net>
 */
class OgDescriptionMetaTag extends AbstractMetaTag
{
	/**
	 * {@inheritdoc}
	 */
	function match( string $document ): ?string
	{
		return $this->matchByProperty( 'og:description', $document );
	}
}