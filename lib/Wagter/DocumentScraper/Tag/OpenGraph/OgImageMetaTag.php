<?php

namespace Wagter\DocumentScraper\Tag\OpenGraph;

use Wagter\DocumentScraper\Tag\AbstractMetaTag;

/**
 * Match a og:image meta tag in a HTML document
 *
 * <meta property="og:image" content="http://url-to-image.com/image.jpg" />
 *
 * Class OgImageMetaTag
 * @package Wagter\DocumentScraper\Tag
 *
 * @author Joris Wagter <http://wagter.net>
 */
class OgImageMetaTag extends AbstractMetaTag
{
	/**
	 * {@inheritdoc}
	 */
	function match( string $document ): ?string
	{
		return $this->matchByProperty( 'og:image', $document );
	}
}