<?php

namespace Wagter\DocumentScraper\Tag\OpenGraph;

use Wagter\DocumentScraper\Tag\AbstractMetaTag;

/**
 * Match a og:title meta tag in a HTML document
 *
 * <meta property="og:title" content="Site Title" />
 *
 * Class OgTitleMetaTag
 * @package Wagter\DocumentScraper\Tag
 *
 * @author Joris Wagter <http://wagter.net>
 */
class OgTitleMetaTag extends AbstractMetaTag
{
	/**
	 * {@inheritdoc}
	 */
	function match( string $document ): ?string
	{
		return $this->matchByProperty( 'og:title', $document );
	}
}