# DocumentScraper
A small PHP library to scrape HTML documents.
 
## Installation
To add to your existing composer project

`composer require wagter/documentscraper`

To edit the package

`git clone https://bitbucket.org/wagter/documentscraper.git`

## Usage

For usage examples, see the [demo directory](https://bitbucket.org/wagter/documentscraper/src/master/demo/).
